-- 2. Create estore tables
DROP SEQUENCE seq_cus_id;
Create sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
  cus_id number not null,
  cus_fname varchar2(15) not null,
  cus_lname varchar2(30) not null,
  cus_street varchar2(30) not null,
  cus_city varchar2(30) not null,
  cus_state char(2) not null,
  cus_zip number(9) not null, --equivalent to number(9,0)
  cus_phone number(10) not null,
  cus_email varchar2(100),
  cus_balance number(7,2),
  cus_notes varchar2(255),
  CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

DROP SEQUENCE seq_com_id; -- for auto increment
Create sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
  com_id number not null,
  com_name varchar2(20),
  com_price NUMBER(8,2) NOT NULL,
  cus_notes varchar2(255),
  CONSTRAINT pk_commodity PRIMARY KEY(com_id),
)