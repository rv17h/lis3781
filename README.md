# LIS3781 - Advanced Database Management

## Rene Valencia

### LIS3781 Requirements:

_Course Work Links:_

1. [A1 README.md](https://bitbucket.org/rv17h/lis3781/src/master/a1/README.md)
	* AMPPS Installation   
	* ERD Model
2. [A2 README.md](https://bitbucket.org/rv17h/lis3781/src/master/a2/README.md)
	* Screenshots of SQL code   
	* Screenshot of populated tables  
	* Bitbucket repo links
3. [A3 README.md](https://bitbucket.org/rv17h/lis3781/src/master/a3/README.md)
	* Create and populate Oracle tables 
	* Screenshot of SQL code  
	* Screenshot of populated tables
4. [A4 README.md](https://bitbucket.org/rv17h/lis3781/src/master/a4/README.md)
	* [Placeholder] 
	* [Placeholder]
5. [A5 README.md](https://bitbucket.org/rv17h/lis3781/src/master/a5/README.md)
	* [Placeholder]    
	* [Placeholder]  
	* [Placeholder] 
6. [P1 README.md](https://bitbucket.org/rv17h/lis3781/src/master/p1/README.md)
	* [Placeholder] 
	* [Placeholder]
7. [P2 README.md](https://bitbucket.org/rv17h/lis3781/src/master/p2/README.md)
	* [Placeholder] 
	* [Placeholder] 