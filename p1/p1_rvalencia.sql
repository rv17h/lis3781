-- ############################## BEGIN INSERTS ##############################

-- -----------------------------
-- Data for person table
-- -----------------------------

START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, 'Steve', 'Rogers', '437 Southern Dnve', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', NULL),
(NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'c', NULL),
(NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL),
(NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmailcom', '1978-05-08', 'c', NULL),
(NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@venzon.net', '1994-07-19', 'c', NULL),
(NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04' , 'a', NULL),
(NULL, NULL, 'Hank', 'Pym', '2355 Brown Street', 'Cleveland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', 'a', NULL),
(NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'soole@gma1l.com', '1990-01-26', 'a', NULL),
(NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotma1l.com', '1983-12-24', 'a', NULL),
(NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gma1l.com', '1975-12-15', 'j', NULL),
(NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dpnce@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, 'Adam', 'Jurns', '98435 Valencia Dr.', 'Gulf Shores' , 'AL', 870219932, 'ajurns@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billngs', 'MT', 672048823, 'jsleen@symaptico.com', '1910-03-22', 'j', NULL),
(NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'beiderheim@comcast.net', '1982-03-13', 'j', NULL);

COMMIT;


-- -----------------------------
-- Data for phone table
-- -----------------------------

START TRANSACTION;
INSERT INTO phone 
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c', NULL),
(NULL, 2, 2052338293, 'h', NULL),
(NULL, 4, 1034325598, 'w', 'has two office nubmers'),
(NULL, 5, 6402338494, 'f', NULL),
(NULL, 6, 5508326842, 'c', 'fax number not currently working'),
(NULL, 7, 8202052203, 'h', 'prefers home calls'),
(NULL, 8, 4008338294, 'w', NULL),
(NULL, 9, 6546513254, 'f', 'work fax number'),
(NULL, 10, 8748613214, 'h', 'prefers cell phone calls'),
(NULL, 11, 4546484212, 'h', NULL),
(NULL, 12, 1816335486, 'w', 'best number to reach'),
(NULL, 13, 4894652164, 'w', 'call during lunch'),
(NULL, 14, 3815698744, 'c', 'prefers cell phone calls'),
(NULL, 15, 2666855948, 'f', 'use for faxing legal documents');

COMMIT;


-- -----------------------------
-- Data for client table
-- -----------------------------
START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;


-- -----------------------------
-- Data for attorney table
-- -----------------------------
START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130,28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;


-- -----------------------------
-- Data for bar table
-- -----------------------------
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida Bar', NULL),
(NULL, 7, 'Alabama Bar', NULL),
(NULL, 8, 'Georgia Bar', NULL),
(NULL, 9, 'Michigan Bar', NULL),
(NULL, 10, 'South Carolina Bar', NULL),
(NULL, 6, 'Montana Bar', NULL),
(NULL, 7, 'Arizona Bar', NULL),
(NULL, 8, 'Nevada Bar', NULL),
(NULL, 9, 'New York Bar', NULL),
(NULL, 10, 'New York Bar', NULL),
(NULL, 6, 'Mississippi Bar', NULL),
(NULL, 7, 'California Bar', NULL),
(NULL, 8, 'Illinois Bar', NULL),
(NULL, 9, 'Indiana Bar', NULL),
(NULL, 10, 'Illinois Bar', NULL),
(NULL, 6, 'Tallahassee Bar', NULL),
(NULL, 7, 'Ocala Bar', NULL),
(NULL, 8, 'Bay County Bar', NULL),
(NULL, 9, 'Cincinatti Bar', NULL);

COMMIT;


-- -----------------------------
-- Data for speciality table
-- -----------------------------
START TRANSACTION;

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptyc', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);

COMMIT;


-- -----------------------------
-- Data for court table
-- -----------------------------
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '301 south monroe street', 'tallahassee', 'fl', 323035292, 8506065504, 'lccc@us.fl.gov', 'http://www.leoncountycircuitcourt.gov/', NULL),
(NULL, 'leoun county traffic court', '1921 thomasville road', 'tallahssee', 'fl', 323035292, 8505774100, 'lctc@us.fl.gov', 'http://www.leoncountytrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '500 south duval street', 'tallahassee', 'fl', 323035292, 8504880125, 'fsc@us.fl.gov', 'http://www.floridasupremecourt.org/', NULL),
(NULL, 'orange county courthouse', '424 north orange avenue', 'orlando', 'fl', 328012248,  4078362000, 'occ@us.fsl.gov', 'http://www.ninthcircuit.org/', NULL),
(NULL, 'fifth district court of appeal', '300 south beach street', 'daytona beach', 'fl', 321158763, 3862258600, '5dca@us.fl.gov', 'http://5dcca.org', NULL);

COMMIT;


-- -----------------------------
-- Data for judge table
-- -----------------------------
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;


-- -----------------------------
-- Data for judge_hist table
-- -----------------------------
START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL),
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL),
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL),
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL),
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL),
(NULL, 14, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(NULL, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(NULL, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice'),
(NULL, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court based upon local area population growth');

COMMIT;


-- -----------------------------
-- Data for case table
-- -----------------------------
START TRANSACTION;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says that his log is being used without his consent to promote a rival business', '2010-09-09', NULL, 'copyright infringement'),
(NULL, 12, 'criminal', 'client is charged with assaulting her husband during an argument', '2009-11-18', '2010-12-23', 'assault'),
(NULL, 14, 'civil', 'client broke an ankle while shopping at a local grocery store. no wet floor sign was posted although the floor had just been mopped', '2008-05-06', '2008-07-23', 'slip and fall'),
(NULL, 11, 'criminal', 'client was charged with stealing several televisions from his former place of employment. client has a solid alibi', '2011-05-20', NULL, 'grand theft'),
(NULL, 13, 'criminal', 'client charged with posession of 10 grams of cocaine, allegedly found in his glove box by state police', '2011-06-05', NULL, 'posession of narcotics'),
(NULL, 14, 'civil', 'client alleges newspaper printed false information about his personal activities while he ran a large laundry business in a small nearby town.', '2007-01-19', '2007-05-20', 'defamation'),
(NULL, 12, 'criminal', 'client charged with the murder of his co-worker over a lovers fued. client has no alibi', '2010-03-20', NULL, 'murder'),
(NULL, 15, 'civil', 'client made the horrible mistake of selecting a degree other than IT and had to declare bankruptcy.', '2012-01-26', '2013-02-28', 'bankruptcy');

COMMIT;


-- -----------------------------
-- Data for assignment table
-- -----------------------------
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);

COMMIT;


-- -----------------------------
-- Procedure for hashing SSN
-- -----------------------------
DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN
    DECLARE x, y INT;
    SET x = 1;
    select count(*) into y from person;

    WHILE x <= y DO
        UPDATE person
        SET per_ssn=(SELECT unhex(sha2(FLOOR(000000001 + (RAND() * 1000000000)), 512)))
        WHERE per_id=x;
    
    SET x = x + 1;

    END WHILE;

END$$
DELIMITER ;
CALL CreatePersonSSN();

SHOW WARNINGS;

SELECT 'show populated per_ssn fields after calling stored proc' as '';
SELECT per_id, LENGTH(per_ssn) FROM person ORDER BY per_id;
DO SLEEP(7);


-- ############################## BEGIN REPORTS ##############################